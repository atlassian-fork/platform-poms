# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [5.1.0] - unreleased

### Changed

- Bumped minor version of `activeobjects` to 3.2.3
    - [BSP-658]: Fixed possible DoS in 3rd party applications query.
    - [BSP-658]: Changed querying for 3rd party applications to group-by query.
    - [BSP-658]: Added dependency on ao-plugin 3.1.7+
    - [BSP-893]: Added support for MySQL 8.0.
- Bumped minor version of `atlassian-streams` to 8.2.0
- Bumped minor version of `application-links` to 7.1.2
- Bumped minor version of `vcache` to 1.13.1
- Bumped minor version of `atlassian-rest` to 6.1.3
- Bumped minor version of `sal` to 4.1.0

### Added
- [BSP-320]: Management of: atlassian-cache-ehcache.
- [BSP-320]: Management of: atlassian-oauth-consumer-core.
- [BSP-320]: Management of: atlassian-plugins-osgi-events.
- [BSP-320]: Management of: fugue-retry, fugue-guava, fugue-scala, fugue-optics.
- [BSP-320]: Management of: atlassian-plugins-webresource-compiler-gcc
- [BSP-958]: Management of: streams-jira-plugin, streams-thirdparty-plugin, streams-jira-inline-actions-plugin.

## [5.0.19] - 2019-12-03

### Fixed
- BSP-777 Reinstated management of `com.atlassian.plugins.rest:com.atlassian.jersey-library:pom` (removed in 5.0.12,
used by Refapp, for example)

## [5.0.18] - 2019-12-03

### Added
- BSP-543 Management of `atlassian-plugins-webresource-api`
- BSP-764 Management of `atlassian-webhooks-api`

### Changed
- BSP-514 Bumped minor version of `atlassian-annotations`
- BSP-516 Bumped minor version of Spring Scanner
- BSP-517 Bumped minor version of `commons-fileupload`
- BSP-520 Bumped minor version of `commons-lang3`

### Fixed
- BSP-508 Bumped bugfix version of `atlassian-oauth`
- BSP-513 Bumped bugfix version of `atlassian-http`
- BSP-515 Bumped bugfix version of `prettyurls`
- BSP-518 Bumped bugfix version of `org.apache.httpcomponents:httpclient`
- BSP-519 Bumped bugfix version of `org.apache.httpcomponents:httpcore`
- BSP-696 Fixed semver violations made in 5.0.12 release, which made major/minor bumps of AO, Applinks, REST, and VCache

## [5.0.17] - 2019-11-13

### Fixed
- Changed the Spring version to one that constitutes a valid Maven GAV

## [5.0.16] - 2019-11-12

### Added
- BSP-468 Management of `dom4j:dom4j`

### Changed
- BSP-468 Bumped minor version of `atlassian-plugins`

## [5.0.15] - 2019-11-05

### Fixed
- BSP-661 Bumped bugfix version of Atlassian trusted apps

## [5.0.14] - 2019-11-01

### Fixed
- Bumped bugfix version of WRM

## [5.0.13] - 2019-10-29

### Fixed
- BSP-610 Bumped bugfix version of `atlassian-rest`
- Bumped bugfix version of AO to one that exists in Maven

## [5.0.12] - 2019-09-19

### Added
- Management of `com.atlassian.gadgets:atlassian-gadgets`

### Changed
- Bumped *major* version of Applinks (reverted in 5.0.18)
- Bumped minor version of AO (reverted in 5.0.18)
- Bumped minor version of REST (reverted in 5.0.18)
- Bumped minor version of VCache (reverted in 5.0.18)
- Removed management of `com.atlassian.plugins.rest:com.atlassian.jersey-library:pom` (reinstated in 5.0.19)

### Fixed
- Bumped bugfix version of Streams

## [5.0.11] - 2019-08-07

### Changed
- VHS-33 Bumped minor version of Velocity HTMLSafe

## [5.0.10] - 2019-08-05

### Fixed
- BSP-504 Bumped micro version of `com.atlassian.jquery:jquery`

## [5.0.9] - 2019-07-08

### Fixed
- DELTA-859 Bumped bugfix version of `atlassian-events`

## [5.0.8] - 2019-07-04

### Fixed
- Bumped bugfix version of WRM

## [5.0.7] - 2019-07-04

### Added
- Management of `com.atlassian.plugins.rest:atlassian-rest-doclet`

### Fixed
- Bumped bugfix version of `atlassian-streams`

## [5.0.6] - 2019-07-03

No changes

## [5.0.5] - 2019-06-05

### Fixed
- DELTA-638 Reverted Spring bump made in 5.0.4
 
## [5.0.4] - 2019-06-04

### Changed
- DELTA-638 Bumped minor version of `atlassian-plugins`

### Fixed
- DELTA-638 Bumped Spring version (reverted in 5.0.5)

## [5.0.3] - 2019-05-21

### Fixed
- BSP-423 Bumped bugfix version of `atlassian-rest`

## [5.0.2] - 2019-04-09

### Changed
- Bumped minor version of WRM

### Fixed
- BSP-370 Bumped bugfix version of `atlassian-rest`

## [5.0.1] - 2019-04-02

### Changed
- Bumped major version of `atlassian-plugins`

## [5.0.0] - 2018-12-14

### Changed
- PLATFORM-213 Support for Platform 5 and Java 11
